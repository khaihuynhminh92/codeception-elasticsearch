<?php

namespace Codeception\Module;

class ElasticSearch extends \Codeception\Module
{
    const CONNECTION_TIMEOUT = 5;
    /**
     * @var \Elastica\Client
     * */
    private $client;

    /**
     * @var array
     * */
    protected $config = [
        'host' => 'localhost',
        'port' => 9200,
        'timeout' => 5
    ];

    public function _initialize()
    {
        $this->validate($this->config);
        $this->client = $this->initializeClient($this->config);
        if (isset($this->config['indexes'])) {
            $this->initializeIndexes($this->config['indexes']);
        }
    }

    private function initializeIndexes($indexes)
    {
        foreach ($indexes as $indexName => $index) {
            $mappingPath = $index['path'];
            $alias = $index['alias'];
            $mapping = file_get_contents($mappingPath);
            $result = (new \Codeception\Module\ElasticSearch\Index\Index($indexName, '', $this->client))
            ->setAlias($alias)
            ->create($mapping);
            if (!$result) {
                throw new \Exception("Cannot create default index $indexName");
            }
        }
    }

    private function initializeClient($config)
    {
        $hosts = explode(',', $config['host']);
        $timeout = array_key_exists('timeout', $config) ? $config['timeout'] : self::CONNECTION_TIMEOUT;
        $port = $config['port'];
        $clusters = [];

        foreach ($hosts as $host) {
            $clusterConfig[] = [
            'host' => $host,
            'port' => $port
            ];
        }
        if ($clusters) {
            $client = new \Elastica\Client([
            'connections' => $clusters,
            'roundRobin' => true,
            'timeout' => $timeout
            ]);
        } else {
            $client = new \Elastica\Client([
            'host' => $config['host'],
            'port' => $port,
            'timeout' => $timeout
            ]);
        }

            return $client;
    }
    public function getConfig()
    {
        return $this->config;
    }
    private function validate($config = array())
    {
        if (!isset($config['host'])) {
            throw new \Exception('Hosts not found');
        }

        if (!isset($config['port'])) {
            throw new \Exception('Port not found');
        }
    }

    /**
     * Index new document to an index
     *
     * @param string $index index name
     * @param string $type item type
     * @param array $data data to index
     *
     * @return array
     */
    public function haveDocInElasticSearch($index, $type, $data)
    {
        if (!isset($data['id'])) {
            throw new \Exception('You cannot have doc without given id');
        }
        $id = $data['id'];
        (new \Codeception\Module\ElasticSearch\Index\Doc\UpdateAction(
            $this->client,
            new \Codeception\Module\ElasticSearch\Index\Doc(
                $id,
                new \Codeception\Module\ElasticSearch\Index\Index($index, $type)
            ),
            $data
        ))->execute();
    }

    public function haveIndexInElasticSearch($indexName, $indexSettings)
    {
        $result = (new \Codeception\Module\ElasticSearch\Index\Index($indexName, '', $this->client))
        ->create($indexSettings);
        if (!$result) {
            throw new \Exception("Cannot create default index $indexName");
        }
    }

    public function dontSeeIndexInElasticSearch($indexName)
    {
        return !(new \Codeception\Module\ElasticSearch\Index\Index($indexName, '', $this->client))
        ->exists();
    }
    
    public function deleteIndexInElasticSearch($indexName)
    {
        $elasticaIndex = new \Codeception\Module\ElasticSearch\Index\Index($indexName, '', $this->client);
        
        if ($elasticaIndex->exists()) {
            return !$elasticaIndex->delete();
        }
    }

    public function reinitializeIndexesInElasticSearch()
    {
        $this->initializeIndexes($this->config['indexes']);
    }
}
