<?php
namespace Codeception\Module\ElasticSearch\Index\Doc;

class UpdateAction
{
    /**
     * @var \Codeception\Module\ElasticSearch\Index\Doc
     * */

    private $doc;

    /**
     * @var array
     * */
    private $data;

    private $client;
    
    public function __construct($client, $doc, $data)
    {
        $this->client = $client;
        $this->doc = $doc;
        $this->data = $data;
    }

    public function execute()
    {
        $index = $this->doc->getIndex();
        $elasticaIndex = $this->client->getIndex($index->getIndex());
        $elasticaType = $elasticaIndex->getType($index->getType());
        $document = new \Elastica\Document($this->doc->getId(), $this->data);
        $elasticaType->addDocument($document);
        $elasticaType->getIndex()->refresh();
    }
}
