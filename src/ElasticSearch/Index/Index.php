<?php
namespace Codeception\Module\ElasticSearch\Index;

class Index
{
    /**
     * @var string
     **/
    private $index;

    /**
     * @var string
     **/
    private $type;
    
    private $client = null;

    private $alias = null;

    public function __construct($index, $type = '', $client = null)
    {
        $this->index = $index;
        $this->type = $type;
        $this->client = $client;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function create($indexSettings)
    {
        if (is_null($this->client)) {
            throw new \Exception('Cannot create index without client');
        }
        $indexSettings = json_decode($indexSettings, true);
        $elasticaIndex = $this->client->getIndex($this->index);
        $elasticaIndex->create($indexSettings['settings']['index'], true);
        
        $type = array_keys($indexSettings['mappings'])[0];
        $this->type = $type;
        
        $elasticaType = $elasticaIndex->getType($type);
        $mapping = new \Elastica\Type\Mapping();
        $mapping->setType($elasticaType);
        $mapping->setProperties($indexSettings['mappings'][$type]['properties']);
        $mapping->send();
        if (!is_null($this->alias)) {
            $result = $this->assignAlias();
            if (!$result) {
                return false;
            }
        }
        return true;
    }
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function assignAlias($alias = null)
    {
        if (is_null($alias) && is_null($this->alias)) {
            throw new \Exception('Cannot assign without alias');
        }
        if (!is_null($alias)) {
            $this->setAlias($alias);
        }

        $elasticaIndex = $this->client->getIndex($this->index);
        return $elasticaIndex->addAlias($this->alias, true);
    }

    public function exists()
    {
        $elasticaIndex = $this->client->getIndex($this->index);
        return $elasticaIndex->exists();
    }

    public function delete()
    {
        $elasticaIndex = $this->client->getIndex($this->index);
        return $elasticaIndex->delete();
    }
}
