<?php
namespace Codeception\Module\ElasticSearch\Index;

class Doc
{
    /**
     * @var string
     * */

    private $id;
     /**
     * @var \Codeception\Module\ElasticSearch\Index\Index
     * */
    private $index;

   
    public function __construct($id, $index)
    {
        $this->id = $id;
        $this->index = $index;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getId()
    {
        return $this->id;
    }
}
